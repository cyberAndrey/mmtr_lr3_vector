import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random r = new Random();
        Vector v1 = new Vector(Arrays.asList(r.nextInt(11), r.nextInt(11), r.nextInt(11)));
        Vector v2 = new Vector(Arrays.asList(r.nextInt(11), r.nextInt(11), r.nextInt(11)));
        System.out.println("Их скалярное произведение " + v1.scalarProduct(v2));
        System.out.println(v1.vectorProduct(v2) + " - это их векторное произведение");
        System.out.println("Косинус угла между ними " + v1.angleBetweenVectors(v2));
        System.out.println(v1.sum(v2) + " - это их сумма");
        System.out.println(v1.difference(v2) + " - это их разность");
        System.out.println("Генерация пяти векторов: ");
        Vector[] vectors = new Vector[5];
        vectors = Vector.generateVectors(5);

    }
}
