import java.lang.reflect.Array;
import java.util.*;

public class Vector {
    private final Integer x;
    private final Integer y;
    private final Integer z;

    public Vector(List<Integer> points) {
        if (points.size() != 3) throw new NullPointerException("Укажите 3 точки");
        this.x = points.get(0);
        this.y = points.get(1);
        this.z = points.get(2);
        System.out.printf("Создан вектор с координатами {%d:%d:%d}\n", x, y, z);
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getZ() {
        return z;
    }

    public Double module() {
        return (Math.sqrt(x*x + y*y + z*z));
    }

    public Integer scalarProduct(Vector vector) {
        return x*vector.getX() + y*vector.getY() + z*vector.getZ();
    }

    public Vector vectorProduct(Vector vector) {
        Integer resX = y*vector.getZ()-z*vector.getY();
        Integer resY = z*vector.getX()-x*vector.getZ();
        Integer resZ = x*vector.getY()-y*vector.getX();
        return new Vector(Arrays.asList(resX, resY, resZ));
    }

    public Double angleBetweenVectors(Vector vector) {
        return this.scalarProduct(vector) / (this.module()*vector.module());
    }

    public Vector sum(Vector vector) {
        return new Vector(Arrays.asList(x+vector.getX(), y+vector.getY(), z+vector.getZ()));
    }

    public Vector difference(Vector vector) {
        return new Vector(Arrays.asList(x-vector.getX(), y-vector.getY(), z-vector.getZ()));
    }

    public static Vector[] generateVectors(Integer n) {
        Random r = new Random();
        Vector[] vectors = new Vector[n];
        for (int i = 0; i < vectors.length; i++) {
            vectors[i] = new Vector(Arrays.asList(r.nextInt(11), r.nextInt(11), r.nextInt(11)));
        }
        return vectors;
    }

    @Override
    public String toString() {
        return "{" + x + ":" + y + ":" + z + "}";
    }
}
